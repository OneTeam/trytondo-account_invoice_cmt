from trytond.pool import Pool, PoolMeta
from trytond.model import fields
from decimal import Decimal
from trytond.modules.product import price_digits
from trytond.transaction import Transaction
from trytond.pyson import If, Eval, Bool
from trytond.exceptions import UserError
from trytond.modules.account.tax import TaxableMixin


class InvoiceLine(metaclass=PoolMeta):
    'Invoice Line'
    __name__ = 'account.invoice.line'

    rental_period = fields.Date('Rental Period',
                                states={
                                    'invisible': Eval('invoice_type') != 'out',
                                    'required': Eval('invoice_type') != 'in',},
                                depends=['invoice_type']
                                )
    rental_days = fields.Numeric('Rental Days',
                                states={
                                    'invisible': Eval('invoice_type') != 'out',
                                    'required': Eval('invoice_type') != 'in',},
                                depends=['invoice_type']
                                )
    unit_price_days = fields.Numeric('Unit Price Days',
                                     digits=price_digits)

    
    @property
    def taxable_lines(self):
        # In case we're called from an on_change we have to use some sensible
        # defaults
        context = Transaction().context
        if (getattr(self, 'invoice', None)
                and getattr(self.invoice, 'type', None)):
            invoice_type = self.invoice.type
        else:
            invoice_type = getattr(self, 'invoice_type', None)
        if invoice_type == 'in':
            unit_price = 'unit_price'
            if context.get('_deductible_rate') is not None:
                deductible_rate = context['_deductible_rate']
            else:
                deductible_rate = getattr(self, 'taxes_deductible_rate', 1)
            if deductible_rate is None:
                deductible_rate = 1
            if not deductible_rate:
                return []
        else:
            deductible_rate = 1
            unit_price = 'unit_price_days'
            
        return [(
                getattr(self, 'taxes', None) or [],
                ((getattr(self, unit_price, None) or Decimal(0))
                    * deductible_rate),
                getattr(self, 'quantity', None) or 0,
                getattr(self, 'tax_date', None),
                )]

    @fields.depends('type', 'quantity', 'unit_price', 'invoice',
                    '_parent_invoice.currency', 'currency', 'rental_days',
                    'unit_price_days', 'invoice_type')
    def on_change_with_amount(self):
        if self.type == 'line':
            rental_days = self.rental_days if self.invoice_type == 'out' else 1
            currency = (self.invoice.currency if self.invoice
                else self.currency)
            amount = (Decimal(str(self.quantity or '0.0'))
                      * (self.unit_price or Decimal('0.0'))
                      * (rental_days or Decimal('0.0')))
            if currency:
                return currency.round(amount)
            return amount
        return Decimal('0.0')

    def get_amount(self, name):
        if self.type == 'line':
            return self.on_change_with_amount()
        elif self.type == 'subtotal':
            subtotal = _ZERO
            for line2 in self.invoice.lines:
                if line2.type == 'line':
                    subtotal += line2.invoice.currency.round(
                        Decimal(str(line2.quantity)) * line2.unit_price)
                elif line2.type == 'subtotal':
                    if self == line2:
                        break
                    subtotal = _ZERO
            return subtotal
        else:
            return _ZERO

    @fields.depends('type', 'unit_price', 'rental_days',
                    'unit_price_days')
    def on_change_unit_price(self):
        if self.type == 'line':
            if self.rental_days:
                self.unit_price_days = (self.unit_price or Decimal('0.0')) \
                * (self.rental_days or Decimal('0.0'))

    @fields.depends('rental_days', 'unit_price_days', 'type',
                    'unit_price')
    def on_change_rental_days(self):
        if self.type == 'line':
            if self.rental_days:
                self.unit_price_days = (self.unit_price or Decimal('0.0')) \
                * (self.rental_days or Decimal('0.0'))
