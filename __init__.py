from trytond.pool import Pool
from .invoice import *
from . sale import *

def register():
    Pool.register(
        InvoiceLine,
        SaleLine,
        module='account_invoice_cmt', type_='model')
