from trytond.pool import Pool, PoolMeta
from trytond.model import fields
from decimal import Decimal
from trytond.modules.product import price_digits
from trytond.transaction import Transaction
from trytond.pyson import If, Eval, Bool
from trytond.exceptions import UserError

    
class SaleLine(metaclass=PoolMeta):
    'Sale Line'
    __name__ = 'sale.line'

    rental_period = fields.Date('Rental Period', required=True)
    rental_days = fields.Numeric('Rental Days', required=True)
    unit_price_days = fields.Numeric('Unit Price Days',
                                     digits=price_digits)

    @property
    def taxable_lines(self):
        # In case we're called from an on_change
        # we have to use some sensible defaults
        if getattr(self, 'type', None) == 'line':
            return [(
                    getattr(self, 'taxes', None) or [],
                    getattr(self, 'unit_price_days', None) or Decimal(0),
                    getattr(self, 'quantity', None) or 0,
                    None,
                    )]
        else:
            return []
    
    @fields.depends('type', 'quantity', 'unit_price', 'unit', 'sale',
                    '_parent_sale.currency', 'rental_days', 'unit_price_days')
    def on_change_with_amount(self):
        if self.type == 'line':
            rental_days = self.rental_days
            currency = (self.sale.currency if self.sale
                else self.currency)
            amount = (Decimal(str(self.quantity or '0.0'))
                      * (self.unit_price or Decimal('0.0'))
                      * (rental_days or Decimal('0.0')))
            if currency:
                return currency.round(amount)
            return amount
        return Decimal('0.0')

    def get_amount(self, name):
        if self.type == 'line':
            return self.on_change_with_amount()
        elif self.type == 'subtotal':
            subtotal = _ZERO
            for line2 in self.sale.lines:
                if line2.type == 'line':
                    subtotal += line2.sale.currency.round(
                        Decimal(str(line2.quantity)) * line2.unit_price)
                elif line2.type == 'subtotal':
                    if self == line2:
                        break
                    subtotal = _ZERO
            return subtotal
        else:
            return _ZERO

    @fields.depends('type', 'unit_price',
                    'rental_days', 'unit_price_days')
    def on_change_unit_price(self):
        if self.type == 'line':
            if self.rental_days:
                self.unit_price_days = (self.unit_price or Decimal('0.0')) \
                * (self.rental_days or Decimal('0.0'))

    @fields.depends('rental_days', 'unit_price_days', 'type',
                    'unit_price')
    def on_change_rental_days(self):
        if self.type == 'line':
            if self.rental_days:
                self.unit_price_days = (self.unit_price or Decimal('0.0')) \
                * (self.rental_days or Decimal('0.0'))

    def get_invoice_line(self):
        'Return a list of invoice lines for sale line'
        pool = Pool()
        InvoiceLine = pool.get('account.invoice.line')
        AccountConfiguration = pool.get('account.configuration')
        account_config = AccountConfiguration(1)

        invoice_line = InvoiceLine()
        invoice_line.type = self.type
        invoice_line.description = self.description
        invoice_line.note = self.note
        invoice_line.origin = self
        if self.type != 'line':
            if self._get_invoice_not_line():
                return [invoice_line]
            else:
                return []
        quantity = (self._get_invoice_line_quantity()
            - self._get_invoiced_quantity())

        if self.unit:
            quantity = self.unit.round(quantity)
        invoice_line.quantity = quantity

        if not invoice_line.quantity:
            return []

        invoice_line.unit = self.unit
        invoice_line.product = self.product
        invoice_line.unit_price = self.unit_price
        invoice_line.taxes = self.taxes
        invoice_line.invoice_type = 'out'
        invoice_line.currency = self.currency
        invoice_line.company = self.company
        invoice_line.rental_days = self.rental_days
        invoice_line.rental_period = self.rental_period
        invoice_line.unit_price_days = self.unit_price_days

        if self.product:
            invoice_line.account = self.product.account_revenue_used
            if not invoice_line.account:
                raise AccountError(
                    gettext('sale.msg_sale_product_missing_account_revenue',
                        sale=self.sale.rec_name,
                        product=self.product.rec_name))
        else:
            invoice_line.account = account_config.get_multivalue(
                'default_category_account_revenue', company=self.company.id)
            if not invoice_line.account:
                raise AccountError(
                    gettext('sale.msg_sale_missing_account_revenue',
                        sale=self.sale.rec_name))
        invoice_line.stock_moves = self._get_invoice_line_moves()
        return [invoice_line]
